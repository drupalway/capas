<?php
/**
 * @file
 * ft_cap_homepage.ds.inc
 */

/**
 * Implements hook_ds_field_settings_info().
 */
function ft_cap_homepage_ds_field_settings_info() {
  $export = array();

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|capas_homepage_item|default';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'capas_homepage_item';
  $ds_fieldsetting->view_mode = 'default';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '1',
      'label' => 'hidden',
      'format' => 'default',
    ),
  );
  $export['node|capas_homepage_item|default'] = $ds_fieldsetting;

  return $export;
}

/**
 * Implements hook_ds_layout_settings_info().
 */
function ft_cap_homepage_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|capas_homepage_item|default';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'capas_homepage_item';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_3col_equal_width';
  $ds_layout->settings = array(
    'regions' => array(
      'left' => array(
        0 => 'field_cap_hp_image',
      ),
      'middle' => array(
        1 => 'title',
        2 => 'body',
      ),
      'right' => array(
        3 => 'field_cap_hp_link',
      ),
    ),
    'fields' => array(
      'field_cap_hp_image' => 'left',
      'title' => 'middle',
      'body' => 'middle',
      'field_cap_hp_link' => 'right',
    ),
    'classes' => array(
      'layout_class' => array(
        '' => '',
      ),
    ),
    'wrappers' => array(
      'left' => 'div',
      'middle' => 'div',
      'right' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|capas_homepage_item|default'] = $ds_layout;

  return $export;
}
