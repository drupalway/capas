<?php
/**
 * @file
 * ft_cap_homepage.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function ft_cap_homepage_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'cap_homepage';
  $context->description = '';
  $context->tag = 'front';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '<front>' => '<front>',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-cap_homepage_block-block' => array(
          'module' => 'views',
          'delta' => 'cap_homepage_block-block',
          'region' => 'highlighted',
          'weight' => '-10',
        ),
      ),
      'layout' => 'basic',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('front');
  $export['cap_homepage'] = $context;

  return $export;
}
