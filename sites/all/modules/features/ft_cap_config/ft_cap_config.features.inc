<?php
/**
 * @file
 * ft_cap_config.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ft_cap_config_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function ft_cap_config_node_info() {
  $items = array(
    'capas_basic_page' => array(
      'name' => t('Page simple'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'capas_event' => array(
      'name' => t('Evénement'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Titre'),
      'help' => '',
    ),
    'capas_job' => array(
      'name' => t('Offre d\'Emploi'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Titre'),
      'help' => '',
    ),
    'capas_news' => array(
      'name' => t('Actualité'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Titre'),
      'help' => '',
    ),
    'capas_review' => array(
      'name' => t('Observatoire et Expertise'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Titre'),
      'help' => '',
    ),
    'capas_training' => array(
      'name' => t('Formation'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Titre'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}

/**
 * Implements hook_default_profile2_type().
 */
function ft_cap_config_default_profile2_type() {
  $items = array();
  $items['capas_member_profile'] = entity_import('profile2_type', '{
    "userCategory" : true,
    "userView" : true,
    "type" : "capas_member_profile",
    "label" : "Profil",
    "weight" : "0",
    "data" : {
      "registration" : 0,
      "roles" : { "2" : "2", "4" : "4", "5" : "5", "3" : "3" },
      "use_one_page" : 1
    },
    "rdf_mapping" : []
  }');
  return $items;
}
