<?php
/**
 * @file
 * ft_cap_config.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function ft_cap_config_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_cap_expert_recommend|node|capas_basic_page|form';
  $field_group->group_name = 'group_cap_expert_recommend';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'capas_basic_page';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Bloc vert',
    'weight' => '6',
    'children' => array(
      0 => 'field_cap_expert_label',
      1 => 'field_cap_expert_recommend',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Bloc vert',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-cap-expert-recommend field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $export['group_cap_expert_recommend|node|capas_basic_page|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_cap_expert_recommend|node|capas_event|form';
  $field_group->group_name = 'group_cap_expert_recommend';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'capas_event';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Bloc vert',
    'weight' => '5',
    'children' => array(
      0 => 'field_cap_expert_label',
      1 => 'field_cap_expert_recommend',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Bloc vert',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-cap-expert-recommend field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $export['group_cap_expert_recommend|node|capas_event|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_cap_expert_recommend|node|capas_job|form';
  $field_group->group_name = 'group_cap_expert_recommend';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'capas_job';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Bloc vert',
    'weight' => '6',
    'children' => array(
      0 => 'field_cap_expert_label',
      1 => 'field_cap_expert_recommend',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Bloc vert',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-cap-expert-recommend field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $export['group_cap_expert_recommend|node|capas_job|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_cap_expert_recommend|node|capas_news|form';
  $field_group->group_name = 'group_cap_expert_recommend';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'capas_news';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Bloc vert',
    'weight' => '5',
    'children' => array(
      0 => 'field_cap_expert_label',
      1 => 'field_cap_expert_recommend',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Bloc vert',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-cap-expert-recommend field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $export['group_cap_expert_recommend|node|capas_news|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_cap_expert_recommend|node|capas_review|form';
  $field_group->group_name = 'group_cap_expert_recommend';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'capas_review';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Bloc vert',
    'weight' => '4',
    'children' => array(
      0 => 'field_cap_expert_recommend',
      1 => 'field_cap_expert_label',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Bloc vert',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-cap-expert-recommend field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsible',
    ),
  );
  $export['group_cap_expert_recommend|node|capas_review|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_cap_expert_recommend|node|capas_training|form';
  $field_group->group_name = 'group_cap_expert_recommend';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'capas_training';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Bloc vert',
    'weight' => '5',
    'children' => array(
      0 => 'field_cap_expert_label',
      1 => 'field_cap_expert_recommend',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Bloc vert',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-cap-expert-recommend field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $export['group_cap_expert_recommend|node|capas_training|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_cap_files|node|capas_basic_page|form';
  $field_group->group_name = 'group_cap_files';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'capas_basic_page';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Fichier(s) joint(s)',
    'weight' => '7',
    'children' => array(
      0 => 'field_cap_files_block_title',
      1 => 'field_fc_cap_files',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Fichier(s) joint(s)',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-cap-files field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $export['group_cap_files|node|capas_basic_page|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_cap_files|node|capas_event|form';
  $field_group->group_name = 'group_cap_files';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'capas_event';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Fichier(s) joint(s)',
    'weight' => '6',
    'children' => array(
      0 => 'field_cap_files_block_title',
      1 => 'field_fc_cap_files',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Fichier(s) joint(s)',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-cap-files field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $export['group_cap_files|node|capas_event|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_cap_files|node|capas_job|form';
  $field_group->group_name = 'group_cap_files';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'capas_job';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Fichier(s) joint(s)',
    'weight' => '7',
    'children' => array(
      0 => 'field_cap_files_block_title',
      1 => 'field_fc_cap_files',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Fichier(s) joint(s)',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-cap-files field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $export['group_cap_files|node|capas_job|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_cap_files|node|capas_news|form';
  $field_group->group_name = 'group_cap_files';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'capas_news';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Fichier(s) joint(s)',
    'weight' => '6',
    'children' => array(
      0 => 'field_cap_files_block_title',
      1 => 'field_fc_cap_files',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Fichier(s) joint(s)',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-cap-files field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $export['group_cap_files|node|capas_news|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_cap_files|node|capas_review|form';
  $field_group->group_name = 'group_cap_files';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'capas_review';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Fichier(s) joint(s)',
    'weight' => '5',
    'children' => array(
      0 => 'field_fc_cap_files',
      1 => 'field_cap_files_block_title',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-cap-files field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_cap_files|node|capas_review|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_cap_files|node|capas_training|form';
  $field_group->group_name = 'group_cap_files';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'capas_training';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Fichier(s) joint(s)',
    'weight' => '6',
    'children' => array(
      0 => 'field_cap_files_block_title',
      1 => 'field_fc_cap_files',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Fichier(s) joint(s)',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-cap-files field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $export['group_cap_files|node|capas_training|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_cap_images|node|capas_basic_page|form';
  $field_group->group_name = 'group_cap_images';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'capas_basic_page';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Galerie d\'images',
    'weight' => '8',
    'children' => array(
      0 => 'field_cap_image',
      1 => 'field_cap_images_block_title',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Galerie d\'images',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-cap-images field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $export['group_cap_images|node|capas_basic_page|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_cap_images|node|capas_event|form';
  $field_group->group_name = 'group_cap_images';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'capas_event';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Galerie d\'images',
    'weight' => '7',
    'children' => array(
      0 => 'field_cap_image',
      1 => 'field_cap_images_block_title',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Galerie d\'images',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-cap-images field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $export['group_cap_images|node|capas_event|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_cap_images|node|capas_job|form';
  $field_group->group_name = 'group_cap_images';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'capas_job';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Galerie d\'images',
    'weight' => '8',
    'children' => array(
      0 => 'field_cap_image',
      1 => 'field_cap_images_block_title',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Galerie d\'images',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-cap-images field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $export['group_cap_images|node|capas_job|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_cap_images|node|capas_news|form';
  $field_group->group_name = 'group_cap_images';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'capas_news';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Galerie d\'images',
    'weight' => '7',
    'children' => array(
      0 => 'field_cap_image',
      1 => 'field_cap_images_block_title',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Galerie d\'images',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-cap-images field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $export['group_cap_images|node|capas_news|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_cap_images|node|capas_review|form';
  $field_group->group_name = 'group_cap_images';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'capas_review';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Galerie d\'images',
    'weight' => '6',
    'children' => array(
      0 => 'field_cap_image',
      1 => 'field_cap_images_block_title',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-cap-images field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_cap_images|node|capas_review|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_cap_images|node|capas_training|form';
  $field_group->group_name = 'group_cap_images';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'capas_training';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Galerie d\'images',
    'weight' => '7',
    'children' => array(
      0 => 'field_cap_image',
      1 => 'field_cap_images_block_title',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Galerie d\'images',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-cap-images field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $export['group_cap_images|node|capas_training|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_cap_members_by_sector|node|capas_review|form';
  $field_group->group_name = 'group_cap_members_by_sector';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'capas_review';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Membre(s) concerné(s)',
    'weight' => '7',
    'children' => array(
      0 => 'field_cap_members_block_title',
      1 => 'field_cap_sector',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-cap-members-by-sector field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_cap_members_by_sector|node|capas_review|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_cap_members|node|capas_basic_page|form';
  $field_group->group_name = 'group_cap_members';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'capas_basic_page';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Membre(s) concerné(s)',
    'weight' => '9',
    'children' => array(
      0 => 'field_cap_member_ref',
      1 => 'field_cap_members_block_title',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Membre(s) concerné(s)',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-cap-members field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $export['group_cap_members|node|capas_basic_page|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_cap_members|node|capas_event|form';
  $field_group->group_name = 'group_cap_members';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'capas_event';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Membre(s) concerné(s)',
    'weight' => '8',
    'children' => array(
      0 => 'field_cap_member_ref',
      1 => 'field_cap_members_block_title',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Membre(s) concerné(s)',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-cap-members field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $export['group_cap_members|node|capas_event|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_cap_members|node|capas_job|form';
  $field_group->group_name = 'group_cap_members';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'capas_job';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Membre(s) concerné(s)',
    'weight' => '9',
    'children' => array(
      0 => 'field_cap_member_ref',
      1 => 'field_cap_members_block_title',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Membre(s) concerné(s)',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-cap-members field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $export['group_cap_members|node|capas_job|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_cap_members|node|capas_news|form';
  $field_group->group_name = 'group_cap_members';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'capas_news';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Membre(s) concerné(s)',
    'weight' => '8',
    'children' => array(
      0 => 'field_cap_member_ref',
      1 => 'field_cap_members_block_title',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Membre(s) concerné(s)',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-cap-members field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $export['group_cap_members|node|capas_news|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_cap_members|node|capas_training|form';
  $field_group->group_name = 'group_cap_members';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'capas_training';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Membre(s) concerné(s)',
    'weight' => '8',
    'children' => array(
      0 => 'field_cap_member_ref',
      1 => 'field_cap_members_block_title',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Membre(s) concerné(s)',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-cap-members field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $export['group_cap_members|node|capas_training|form'] = $field_group;

  return $export;
}
