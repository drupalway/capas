<?php
/**
 * @file
 * ft_cap_views.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ft_cap_views_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "boxes" && $api == "box") {
    return array("version" => "1");
  }
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function ft_cap_views_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function ft_cap_views_image_default_styles() {
  $styles = array();

  // Exported image style: image_big.
  $styles['image_big'] = array(
    'label' => 'image_big',
    'effects' => array(
      15 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 900,
          'height' => 900,
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: image_block.
  $styles['image_block'] = array(
    'label' => 'image_block',
    'effects' => array(
      14 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 241,
          'height' => 138,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: member_detail.
  $styles['member_detail'] = array(
    'label' => 'member_detail',
    'effects' => array(
      3 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 240,
          'height' => 145,
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: member_icon.
  $styles['member_icon'] = array(
    'label' => 'member_icon',
    'effects' => array(
      1 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 90,
          'height' => 45,
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
      4 => array(
        'name' => 'image_desaturate',
        'data' => array(),
        'weight' => 2,
      ),
    ),
  );

  // Exported image style: member_overview.
  $styles['member_overview'] = array(
    'label' => 'member_overview',
    'effects' => array(
      16 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 240,
          'height' => 145,
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: member_relevant_block.
  $styles['member_relevant_block'] = array(
    'label' => 'member_relevant_block',
    'effects' => array(
      13 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 217,
          'height' => '',
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}
