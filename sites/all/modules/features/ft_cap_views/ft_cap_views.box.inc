<?php
/**
 * @file
 * ft_cap_views.box.inc
 */

/**
 * Implements hook_default_box().
 */
function ft_cap_views_default_box() {
  $export = array();

  $box = new stdClass();
  $box->disabled = FALSE; /* Edit this to true to make a default box disabled initially */
  $box->api_version = 1;
  $box->delta = 'footer_left';
  $box->plugin_key = 'simple';
  $box->title = '';
  $box->description = 'Footer left content';
  $box->options = array(
    'body' => array(
      'value' => '<span class="top">CAPAS - Collectif d\'Associations
Pour l\'Action Sociale</span>

Case 128 - 1211 Genève 7
secretariat@capas-ge.ch',
      'format' => 'filtered_html',
    ),
    'additional_classes' => 'col col-md-4',
  );
  $export['footer_left'] = $box;

  $box = new stdClass();
  $box->disabled = FALSE; /* Edit this to true to make a default box disabled initially */
  $box->api_version = 1;
  $box->delta = 'footer_right';
  $box->plugin_key = 'simple';
  $box->title = '';
  $box->description = 'Footer copyright';
  $box->options = array(
    'body' => array(
      'value' => 'copyright © 2015 - Intranet',
      'format' => 'filtered_html',
    ),
    'additional_classes' => 'col col-md-3 col-md-offset-5',
  );
  $export['footer_right'] = $box;

  return $export;
}
