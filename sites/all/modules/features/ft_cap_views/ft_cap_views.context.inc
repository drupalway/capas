<?php
/**
 * @file
 * ft_cap_views.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function ft_cap_views_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'capas_event';
  $context->description = '';
  $context->tag = 'node';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'capas_event' => 'capas_event',
      ),
      'options' => array(
        'node_form' => '0',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-relevant_content-block_6' => array(
          'module' => 'views',
          'delta' => 'relevant_content-block_6',
          'region' => 'post_content',
          'weight' => '-10',
        ),
        'views-relevant_content-block_2' => array(
          'module' => 'views',
          'delta' => 'relevant_content-block_2',
          'region' => 'post_content',
          'weight' => '-9',
        ),
        'views-relevant_content-block_7' => array(
          'module' => 'views',
          'delta' => 'relevant_content-block_7',
          'region' => 'post_content',
          'weight' => '-8',
        ),
        'views-relevant_content-block_8' => array(
          'module' => 'views',
          'delta' => 'relevant_content-block_8',
          'region' => 'post_content',
          'weight' => '-7',
        ),
        'views-relevant_content-block_9' => array(
          'module' => 'views',
          'delta' => 'relevant_content-block_9',
          'region' => 'post_content',
          'weight' => '-6',
        ),
        'views-relevant_content-block_3' => array(
          'module' => 'views',
          'delta' => 'relevant_content-block_3',
          'region' => 'post_content',
          'weight' => '-5',
        ),
        'cap_common-back_to_list' => array(
          'module' => 'cap_common',
          'delta' => 'back_to_list',
          'region' => 'sidebar_first',
          'weight' => '-10',
        ),
        'views-relevant_content-block_4' => array(
          'module' => 'views',
          'delta' => 'relevant_content-block_4',
          'region' => 'sidebar_first',
          'weight' => '-9',
        ),
        'views-relevant_content-block' => array(
          'module' => 'views',
          'delta' => 'relevant_content-block',
          'region' => 'sidebar_first',
          'weight' => '-8',
        ),
        'views-relevant_content-block_1' => array(
          'module' => 'views',
          'delta' => 'relevant_content-block_1',
          'region' => 'sidebar_first',
          'weight' => '-7',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('node');
  $export['capas_event'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'capas_job';
  $context->description = '';
  $context->tag = 'node';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'capas_job' => 'capas_job',
      ),
      'options' => array(
        'node_form' => '1',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-relevant_content-block_6' => array(
          'module' => 'views',
          'delta' => 'relevant_content-block_6',
          'region' => 'post_content',
          'weight' => '-26',
        ),
        'views-relevant_content-block_2' => array(
          'module' => 'views',
          'delta' => 'relevant_content-block_2',
          'region' => 'post_content',
          'weight' => '-25',
        ),
        'views-relevant_content-block_7' => array(
          'module' => 'views',
          'delta' => 'relevant_content-block_7',
          'region' => 'post_content',
          'weight' => '-24',
        ),
        'views-relevant_content-block_8' => array(
          'module' => 'views',
          'delta' => 'relevant_content-block_8',
          'region' => 'post_content',
          'weight' => '-23',
        ),
        'views-relevant_content-block_9' => array(
          'module' => 'views',
          'delta' => 'relevant_content-block_9',
          'region' => 'post_content',
          'weight' => '-22',
        ),
        'views-relevant_content-block_3' => array(
          'module' => 'views',
          'delta' => 'relevant_content-block_3',
          'region' => 'post_content',
          'weight' => '-21',
        ),
        'cap_common-back_to_list' => array(
          'module' => 'cap_common',
          'delta' => 'back_to_list',
          'region' => 'sidebar_first',
          'weight' => '-10',
        ),
        'views-relevant_content-block_4' => array(
          'module' => 'views',
          'delta' => 'relevant_content-block_4',
          'region' => 'sidebar_first',
          'weight' => '-9',
        ),
        'views-relevant_content-block' => array(
          'module' => 'views',
          'delta' => 'relevant_content-block',
          'region' => 'sidebar_first',
          'weight' => '-8',
        ),
        'views-relevant_content-block_1' => array(
          'module' => 'views',
          'delta' => 'relevant_content-block_1',
          'region' => 'sidebar_first',
          'weight' => '-7',
        ),
      ),
      'layout' => 'basic',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('node');
  $export['capas_job'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'capas_news';
  $context->description = '';
  $context->tag = 'node';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'capas_news' => 'capas_news',
      ),
      'options' => array(
        'node_form' => '1',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-relevant_content-block_6' => array(
          'module' => 'views',
          'delta' => 'relevant_content-block_6',
          'region' => 'post_content',
          'weight' => '-26',
        ),
        'views-relevant_content-block_2' => array(
          'module' => 'views',
          'delta' => 'relevant_content-block_2',
          'region' => 'post_content',
          'weight' => '-25',
        ),
        'views-relevant_content-block_7' => array(
          'module' => 'views',
          'delta' => 'relevant_content-block_7',
          'region' => 'post_content',
          'weight' => '-24',
        ),
        'views-relevant_content-block_8' => array(
          'module' => 'views',
          'delta' => 'relevant_content-block_8',
          'region' => 'post_content',
          'weight' => '-23',
        ),
        'views-relevant_content-block_9' => array(
          'module' => 'views',
          'delta' => 'relevant_content-block_9',
          'region' => 'post_content',
          'weight' => '-22',
        ),
        'views-relevant_content-block_3' => array(
          'module' => 'views',
          'delta' => 'relevant_content-block_3',
          'region' => 'post_content',
          'weight' => '-21',
        ),
        'cap_common-back_to_list' => array(
          'module' => 'cap_common',
          'delta' => 'back_to_list',
          'region' => 'sidebar_first',
          'weight' => '-10',
        ),
        'views-relevant_content-block_4' => array(
          'module' => 'views',
          'delta' => 'relevant_content-block_4',
          'region' => 'sidebar_first',
          'weight' => '-9',
        ),
        'views-relevant_content-block' => array(
          'module' => 'views',
          'delta' => 'relevant_content-block',
          'region' => 'sidebar_first',
          'weight' => '-8',
        ),
        'views-relevant_content-block_1' => array(
          'module' => 'views',
          'delta' => 'relevant_content-block_1',
          'region' => 'sidebar_first',
          'weight' => '-7',
        ),
      ),
      'layout' => 'basic',
    ),
    'menu' => array(
      0 => 'main-menu:news',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('node');
  $export['capas_news'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'capas_review';
  $context->description = '';
  $context->tag = 'node';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'capas_review' => 'capas_review',
      ),
      'options' => array(
        'node_form' => '1',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-relevant_content-block_6' => array(
          'module' => 'views',
          'delta' => 'relevant_content-block_6',
          'region' => 'post_content',
          'weight' => '-10',
        ),
        'views-relevant_content-block_2' => array(
          'module' => 'views',
          'delta' => 'relevant_content-block_2',
          'region' => 'post_content',
          'weight' => '-9',
        ),
        'views-relevant_content-block_7' => array(
          'module' => 'views',
          'delta' => 'relevant_content-block_7',
          'region' => 'post_content',
          'weight' => '-8',
        ),
        'views-relevant_content-block_8' => array(
          'module' => 'views',
          'delta' => 'relevant_content-block_8',
          'region' => 'post_content',
          'weight' => '-7',
        ),
        'views-relevant_content-block_5' => array(
          'module' => 'views',
          'delta' => 'relevant_content-block_5',
          'region' => 'post_content',
          'weight' => '-6',
        ),
        'views-cap_members-block_1' => array(
          'module' => 'views',
          'delta' => 'cap_members-block_1',
          'region' => 'post_content',
          'weight' => '-5',
        ),
        'menu_block-2' => array(
          'module' => 'menu_block',
          'delta' => '2',
          'region' => 'sidebar_first',
          'weight' => '-10',
        ),
        'views-relevant_content-block' => array(
          'module' => 'views',
          'delta' => 'relevant_content-block',
          'region' => 'sidebar_first',
          'weight' => '-9',
        ),
        'views-relevant_content-block_1' => array(
          'module' => 'views',
          'delta' => 'relevant_content-block_1',
          'region' => 'sidebar_first',
          'weight' => '-8',
        ),
      ),
      'layout' => 'basic',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('node');
  $export['capas_review'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'capas_training';
  $context->description = '';
  $context->tag = 'node';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'capas_training' => 'capas_training',
      ),
      'options' => array(
        'node_form' => '1',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-relevant_content-block_6' => array(
          'module' => 'views',
          'delta' => 'relevant_content-block_6',
          'region' => 'post_content',
          'weight' => '-26',
        ),
        'views-relevant_content-block_2' => array(
          'module' => 'views',
          'delta' => 'relevant_content-block_2',
          'region' => 'post_content',
          'weight' => '-25',
        ),
        'views-relevant_content-block_7' => array(
          'module' => 'views',
          'delta' => 'relevant_content-block_7',
          'region' => 'post_content',
          'weight' => '-24',
        ),
        'views-relevant_content-block_8' => array(
          'module' => 'views',
          'delta' => 'relevant_content-block_8',
          'region' => 'post_content',
          'weight' => '-23',
        ),
        'views-relevant_content-block_9' => array(
          'module' => 'views',
          'delta' => 'relevant_content-block_9',
          'region' => 'post_content',
          'weight' => '-22',
        ),
        'views-relevant_content-block_3' => array(
          'module' => 'views',
          'delta' => 'relevant_content-block_3',
          'region' => 'post_content',
          'weight' => '-21',
        ),
        'cap_common-back_to_list' => array(
          'module' => 'cap_common',
          'delta' => 'back_to_list',
          'region' => 'sidebar_first',
          'weight' => '-10',
        ),
        'views-relevant_content-block_4' => array(
          'module' => 'views',
          'delta' => 'relevant_content-block_4',
          'region' => 'sidebar_first',
          'weight' => '-9',
        ),
        'views-relevant_content-block' => array(
          'module' => 'views',
          'delta' => 'relevant_content-block',
          'region' => 'sidebar_first',
          'weight' => '-8',
        ),
        'views-relevant_content-block_1' => array(
          'module' => 'views',
          'delta' => 'relevant_content-block_1',
          'region' => 'sidebar_first',
          'weight' => '-7',
        ),
      ),
      'layout' => 'basic',
    ),
    'menu' => array(
      0 => 'main-menu:formations',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('node');
  $export['capas_training'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'global';
  $context->description = '';
  $context->tag = 'global';
  $context->conditions = array(
    'sitewide' => array(
      'values' => array(
        1 => 1,
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'system-main-menu' => array(
          'module' => 'system',
          'delta' => 'main-menu',
          'region' => 'navigation',
          'weight' => '-9',
        ),
        'sharethis-sharethis_block' => array(
          'module' => 'sharethis',
          'delta' => 'sharethis_block',
          'region' => 'content',
          'weight' => '-10',
        ),
        'system-main' => array(
          'module' => 'system',
          'delta' => 'main',
          'region' => 'content',
          'weight' => '-9',
        ),
        'boxes-footer_left' => array(
          'module' => 'boxes',
          'delta' => 'footer_left',
          'region' => 'footer',
          'weight' => '-10',
        ),
        'boxes-footer_right' => array(
          'module' => 'boxes',
          'delta' => 'footer_right',
          'region' => 'footer',
          'weight' => '-9',
        ),
      ),
      'layout' => 'basic',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('global');
  $export['global'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'member';
  $context->description = '';
  $context->tag = 'profile2';
  $context->conditions = array(
    'user_page' => array(
      'values' => array(
        'view' => 'view',
      ),
      'options' => array(
        'mode' => 'all',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-cap_news-block_1' => array(
          'module' => 'views',
          'delta' => 'cap_news-block_1',
          'region' => 'post_content',
          'weight' => '-10',
        ),
        'views-cap_events-block_1' => array(
          'module' => 'views',
          'delta' => 'cap_events-block_1',
          'region' => 'post_content',
          'weight' => '-9',
        ),
        'views--exp-cap_members-page_1' => array(
          'module' => 'views',
          'delta' => '-exp-cap_members-page_1',
          'region' => 'sidebar_first',
          'weight' => '-10',
        ),
        'views-cap_jobs-block_1' => array(
          'module' => 'views',
          'delta' => 'cap_jobs-block_1',
          'region' => 'sidebar_first',
          'weight' => '-9',
        ),
      ),
      'layout' => 'basic',
    ),
    'menu' => array(
      0 => 'menu-top-menu:membres',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('profile2');
  $export['member'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'page_simple';
  $context->description = '';
  $context->tag = 'node';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'capas_basic_page' => 'capas_basic_page',
      ),
      'options' => array(
        'node_form' => '1',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-relevant_content-block_6' => array(
          'module' => 'views',
          'delta' => 'relevant_content-block_6',
          'region' => 'post_content',
          'weight' => '-10',
        ),
        'views-relevant_content-block_2' => array(
          'module' => 'views',
          'delta' => 'relevant_content-block_2',
          'region' => 'post_content',
          'weight' => '-9',
        ),
        'views-relevant_content-block_7' => array(
          'module' => 'views',
          'delta' => 'relevant_content-block_7',
          'region' => 'post_content',
          'weight' => '-8',
        ),
        'views-relevant_content-block_8' => array(
          'module' => 'views',
          'delta' => 'relevant_content-block_8',
          'region' => 'post_content',
          'weight' => '-7',
        ),
        'views-relevant_content-block_9' => array(
          'module' => 'views',
          'delta' => 'relevant_content-block_9',
          'region' => 'post_content',
          'weight' => '-6',
        ),
        'views-relevant_content-block_3' => array(
          'module' => 'views',
          'delta' => 'relevant_content-block_3',
          'region' => 'post_content',
          'weight' => '-5',
        ),
        'menu_block-1' => array(
          'module' => 'menu_block',
          'delta' => '1',
          'region' => 'sidebar_first',
          'weight' => '-10',
        ),
        'menu_block-3' => array(
          'module' => 'menu_block',
          'delta' => '3',
          'region' => 'sidebar_first',
          'weight' => '-9',
        ),
        'views-relevant_content-block' => array(
          'module' => 'views',
          'delta' => 'relevant_content-block',
          'region' => 'sidebar_first',
          'weight' => '-8',
        ),
        'views-relevant_content-block_1' => array(
          'module' => 'views',
          'delta' => 'relevant_content-block_1',
          'region' => 'sidebar_first',
          'weight' => '-7',
        ),
      ),
      'layout' => 'basic',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('node');
  $export['page_simple'] = $context;

  return $export;
}
