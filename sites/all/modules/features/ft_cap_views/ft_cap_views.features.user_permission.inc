<?php
/**
 * @file
 * ft_cap_views.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function ft_cap_views_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'access navbar'.
  $permissions['access navbar'] = array(
    'name' => 'access navbar',
    'roles' => array(
      'administrator' => 'administrator',
      'member' => 'member',
      'owner' => 'owner',
    ),
    'module' => 'navbar',
  );

  // Exported permission: 'edit views basic settings'.
  $permissions['edit views basic settings'] = array(
    'name' => 'edit views basic settings',
    'roles' => array(
      'administrator' => 'administrator',
      'owner' => 'owner',
    ),
    'module' => 'views_ui_basic',
  );

  return $permissions;
}
