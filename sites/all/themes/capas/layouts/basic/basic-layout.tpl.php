<div id="mobile-menu-bg">
  <?php print $secondary_nav = render($secondary_nav); ?>
  <?php print $primary_nav = render($primary_nav); ?>
</div>
<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
  <span class="sr-only">Toggle navigation</span>
</button>
<div id="top-fixed-gap"></div>
<header id="header">
  <div class="container">
    <div class="row">
      <div class="l-branding col col-md-5 col-sm-9">
        <div class="branding-inner-wrapper">
          <?php if ($logo): ?>
            <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" class="site-logo"><img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" /></a>
          <?php endif; ?>

          <?php if ($site_name || $site_slogan): ?>
            <?php if ($site_name): ?>
              <h1 class="site-name">
                <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><span><?php print $site_name; ?></span></a>
              </h1>
            <?php endif; ?>

            <?php if ($site_slogan): ?>
              <h2 class="site-slogan"><?php print $site_slogan; ?></h2>
            <?php endif; ?>
          <?php endif; ?>

          <?php print render($page['branding']); ?>
        </div>
      </div>
      <div class="col col-md-7 col-sm-3">
        <div class="navbar-collapse collapse">
          <nav role="navigation">
            <?php print render($secondary_nav); ?>
          </nav>
        </div>
      </div>
    </div>
  </div>
</header>

<?php //dsm($variables); ?>
<div id="navigation">
  <div class="container">
    <div class="row">
      <div class="col col-md-12">
        <div class="navbar-collapse collapse">
          <nav role="navigation">
            <?php print render($primary_nav); ?>
          </nav>
        </div>
      </div>
    </div>
  </div>
</div>

<div<?php print $attributes; ?>>

  <div class="container">
    <div class="row">
      <?php if (!empty($page['sidebar_first'])): ?>
        <div class="col col-md-4">
          <?php print render($page['sidebar_first']); ?>
        </div>
      <?php endif; ?>
      <div class="l-content<?php !empty($page['sidebar_first']) ? print ' col col-md-8': print ' col col-md-12'; ?>" role="main">
        <div class="l-content-inner-wrapper">
          <?php print render($page['highlighted']); ?>
          <?php //print $breadcrumb; ?>
          <a id="main-content"></a>
          <?php print render($title_prefix); ?>
          <?php if ($title): ?>
            <h1 class="main-page-title"><?php print $title; ?></h1>
          <?php endif; ?>
          <?php print render($title_suffix); ?>
          <?php print $messages; ?>
          <?php print render($tabs); ?>
          <?php print render($page['help']); ?>
          <?php if ($action_links): ?>
            <ul class="action-links"><?php print render($action_links); ?></ul>
          <?php endif; ?>
          <?php print render($page['content']); ?>
          <?php print $feed_icons; ?>
        </div>
      </div>
    </div>

    <?php //print render($page['sidebar_second']); ?>
  </div>
  <?php if (!empty($page['post_content'])): ?>
    <div class="container">
      <div class="row">
        <div class="post-content-inner-wrapper">
          <?php print render($page['post_content']); ?>
        </div>
      </div>
    </div>
  <?php endif; ?>
</div>

<?php /* region--footer.tpl.php */ ?>
<?php print render($page['footer']); ?>
