(function ($) {
  /**
   * Helper behavior
   *
   * @type {{attach: Function}}
   */
  Drupal.behaviors.capasGlobal = {
    attach: function (context) {
      $('.role-member #navbar-link-admin-config').closest('li').hide();
      $('.role-member #navbar-link-admin-structure').closest('li').hide();
      $('.role-member #navbar-item--3').hide();
      $('.role-owner #navbar-item--3').hide();
      $('.role-owner #navbar-link-admin-config').closest('li').hide();
      $('.role-owner #navbar-link-admin-structure-menu-manage-management').closest('li').hide();
      $('.role-owner #navbar-link-admin-structure-menu-manage-navigation').closest('li').hide();
      $('.role-owner #navbar-link-admin-structure-menu-manage-user-menu').closest('li').hide();
      $('.role-owner a[href="/admin/structure/taxonomy/add"]').hide();
      $('#navbar-item--2').click(function() {
        window.location.href = 'http://' + document.domain + Drupal.settings.basePath + 'admin/content';
      });
    }
  };
})(jQuery);
