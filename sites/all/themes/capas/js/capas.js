(function ($) {
  /**
   * Helper behavior
   *
   * @type {{attach: Function}}
   */
  Drupal.behaviors.capasCustom = {
    attach: function(context) {

      var heights = $(".front .col-height").map(function() {
          return $(this).height();
        }).get(),

        maxHeight = Math.max.apply(null, heights);

      $(".front .col-height").height(maxHeight);

      // Select for bootstrap select
      $('.views-exposed-form #edit-member-wrapper .form-control.form-select').selectpicker();
      $('.views-exposed-form #edit-year-wrapper .form-control.form-select').selectpicker();
      $('.views-exposed-form #edit-sector-wrapper .form-control.form-select').selectpicker();

      // Change region for page titles on overview pages
      $('body.page-membres h1.main-page-title').prependTo('.view-cap-members .view-filters-inner-wrapper');
      $('body.page-jobs h1.main-page-title').prependTo('.view-cap-jobs .grid-page-title');
      $('body.page-formations h1.main-page-title').prependTo('.view-cap-trainings .grid-page-title');
      $('body.page-formations .view-cap-trainings .view-header').prependTo('.view-cap-trainings .header-text');
      $('body.page-news h1.main-page-title').prependTo('.view-cap-news .view-filters-inner-wrapper');
      $('body.page-events h1.main-page-title').prependTo('.view-cap-events .grid-page-title');

      // Move Member logo on detail page
      //$('.view-relevant-content.view-display-id-block_4').prependTo('.page-node- .l-content-inner-wrapper');
      //$appendTo('.node-type-capas-news .l-content-inner-wrapper');
      //$appendTo('.node-type-capas-training .l-content-inner-wrapper');

      // Redirect to profile page from member overview page
      //console.log('aaa');
      $('.view-cap-members .view-filters .form-control#edit-member').change(function() {
        window.location.href = 'http://' + document.domain + Drupal.settings.basePath + 'capas-profile2/' + this.value;
        $(this).val('All');
        return false;
      });

      /*$('.site-logo img').click(function() {
        window.location.href = 'http://' + document.domain + '/';
      });*/

      // Redirect to profile page from member detail page
      $('#block-views-exp-cap-members-page-1 .form-control#edit-member').change(function() {
        window.location.href = 'http://' + document.domain + Drupal.settings.basePath + 'capas-profile2/' + this.value;
        $(this).val('All');
        return false;
      });

      // Redirect to members page with selected sector
      $('#block-views-exp-cap-members-page-1 .form-control#edit-sector').change(function() {
        window.location.href = 'http://' + document.domain + Drupal.settings.basePath + 'membres?member=All&sector=' + this.value;
        $(this).val('All');
        return false;
      });

      // Replace first item in year filter
      $('select#edit-year-value-year > option:first-child').text('Année');

      // Click event on back to list link
      $('.back-to-list').click(function() {
        if ($(this).find('a').attr('href').indexOf('http://') > -1) {
          window.location.href = $(this).find('a').attr('href');
        }
        else {
          window.location.href = 'http://' + document.domain + $(this).find('a').attr('href');
        }
      });

      // Click event on relevant member tile
      $('.view-id-relevant_content.view-display-id-block_4').click(function() {
        window.location.href = 'http://' + document.domain + $(this).find('a').attr('href');
      });

      // Click events on tile

      // News
      $('.news-inner-wrapper').click(function() {
        window.location.href = 'http://' + document.domain + $(this).find('.views-field-title a').attr('href');
      });

      // Trainings
      $('.training-inner-wrapper').click(function() {
        window.location.href = 'http://' + document.domain + $(this).find('.views-field-title a').attr('href');
      });

      // Events
      //$('.view-cap-events .views-field-body').hide();
      $('div.event-inner-wrapper').unbind('click').bind('click', function (e) {
        if ($(this).hasClass('show-body')) {
          $(this).removeClass('show-body');
          $(this).css('overflow-y', 'visible').find('.event-inner-wrapper-hover').css('overflow-y', 'auto').css('z-index', '98').css('border', '2px solid #f9b322').animate({height:87}, 300, function() {
            $(this).css('border', '0');
            $(this).css('z-index', 'auto');
            $(this).css('overflow-y', 'hidden');
            $(this).animate({ scrollTop: 0 }, 'fast');
            $(this).closest('.event-inner-wrapper').css('overflow-y', 'hidden');
            $(this).closest('.event-inner-wrapper').find('.close-icon').hide();
            $(this).closest('.event-inner-wrapper').find('.views-field-body').hide();
          });
          return false;
        }
        else {
          $('.show-body .event-inner-wrapper-hover').css('overflow-y', 'auto').css('z-index', '98').css('border', '2px solid #f9b322').animate({height:87}, 300, function() {
            $(this).css('border', '0');
            $(this).css('z-index', 'auto');
            $(this).css('overflow-y', 'hidden');
            $(this).closest('.event-inner-wrapper').css('overflow-y', 'hidden');
            $(this).closest('.event-inner-wrapper').find('.close-icon').hide();
            $(this).animate({ scrollTop: 0 }, 'fast').find('.views-field-body').hide();
          });
          $('div.event-inner-wrapper').removeClass('show-body');
          //$(this).find('.views-field-body').css('width', $(this).css('width'));
          $(this).addClass('show-body');
          $(this).find('.close-icon').show();
          $(this).find('.views-field-body').show();
          $(this).css('overflow-y', 'visible').find('.event-inner-wrapper-hover').css('border', '2px solid #f9b322').css('overflow-y', 'auto').css('z-index', '98').animate({height:365}, 500, function() {

          });
        }
      });

      $('.event-inner-wrapper .views-field-title a').unbind('click').bind('click', function (e) {
        $(this).closest('.event-inner-wrapper').click();
        /*if ($(this).closest('.event-inner-wrapper').hasClass('show-body')) {
          $(this).closest('.event-inner-wrapper').removeClass('show-body');
          $(this).closest('.event-inner-wrapper').find('.close-icon').hide();
          //$(this).closest('.event-inner-wrapper').find('.event-inner-wrapper-hover').slideUp('fast');
        }
        else {
          $('.event-inner-wrapper').removeClass('show-body');
          //$('.views-field-body').slideUp('show-body');
          $('.close-icon').hide();
          //$(this).closest('.event-inner-wrapper').find('.views-field-body').css('width', $(this).closest('.event-inner-wrapper').css('width'));
          $(this).closest('.event-inner-wrapper').addClass('show-body');
          $(this).closest('.event-inner-wrapper').find('.close-icon').show();
          $(this).closest('.event-inner-wrapper').find('.event-inner-wrapper-hover').animate({height:"toggle"});
        }*/
        return false;
      });

      // Jobs
      $('.job-inner-wrapper').click(function() {
        window.location.href = 'http://' + document.domain + $(this).find('.views-field-title a').attr('href');
      });

      // Files
      $('.view-relevant-content .views-field-field-fc-cap-files').click(function() {
        window.location.href = $(this).find('.field--name-field-cap-file a').attr('href');
      });

      // Members
      $('.view-relevant-content .field--name-field-cap-logo').click(function() {
        window.location.href = 'http://' + document.domain + $(this).find('.field__item a').attr('href');
      });

      // Members
      $('.view-cap-members .views-field-field-cap-logo').click(function() {
        window.location.href = 'http://' + document.domain + $(this).find('.field-content a').attr('href');
      });

      // Add element to dropdown menu

      $('#navigation li.dropdown.expanded').prepend('<div class="dropdown-border"></div>');
      $('header#header ul.menu li.dropdown.expanded').prepend('<div class="dropdown-border"></div>');

      // Fixed top menu.
      $(window).bind('scroll', function() {
        if ($(window).scrollTop() > 80) {
          $('header#header').addClass('resize');
          $('#navigation').addClass('resize');
          $('button.navbar-toggle').addClass('resize');
        }
        else {
          $('header#header').removeClass('resize');
          $('#navigation').removeClass('resize');
          $('#navigation').removeClass('resize');
          $('button.navbar-toggle').removeClass('resize');
        }
      });

      // Green buttons and toggle menu button.
      function placeOnRightHandEdgeOfElement(toolbar, pageElement, horizontalGap, displayBlock) {
        var pageElementWidth = $(pageElement).width();
        pageElementWidth = pageElementWidth + horizontalGap;
        $(toolbar).css("right", $(window).scrollLeft() + $(window).width()
        - $(pageElement).offset().left
        - parseInt($(pageElement).css("borderLeftWidth"),10)
        - pageElementWidth + "px");
        if (displayBlock) {
          $(toolbar).css('display', 'block');
        }
      }

      placeOnRightHandEdgeOfElement(".page-icons-wrapper", ".l-content", 38, true);

      $(window).resize(function() {
        placeOnRightHandEdgeOfElement(".page-icons-wrapper", ".l-content", 38, true);
      });
      $(window).scroll(function () {
        placeOnRightHandEdgeOfElement(".page-icons-wrapper", ".l-content", 38, true);
      });

      placeOnRightHandEdgeOfElement("button.navbar-toggle", "header#header .row", -6, false);

      $(window).resize(function() {
        placeOnRightHandEdgeOfElement("button.navbar-toggle", "header#header .row", -6, false);
      });
      $(window).scroll(function () {
        placeOnRightHandEdgeOfElement("button.navbar-toggle", "header#header .row", -6, false);
      });

      // Scroll to top
      $('.page-top-wrapper').click(function() {
        $('html, body').animate({ scrollTop: 0 }, 'fast');
        return false;
      });

      // Print page
      $('.page-print-wrapper').click(function() {
        window.print();
      });

      // Mobile menu toggle
      $('.navbar-toggle').click(function() {
        if ($(this).hasClass('collapsed')) {
          $('#mobile-menu-bg').css('display', 'block');
          $('html, body').animate({ scrollTop: 0 }, 'fast');
        }
        else {
          $('#mobile-menu-bg').css('display', 'none');
        }
      });

      // Set min-width for dropdown menu element
      $('ul.menu > li.dropdown').each(function( index ) {
        $(this).find('ul.dropdown-menu').css('min-width', '100%');
      });

      // Remove parameter on active icon
      $("#edit-type-wrapper a.active[href*='type=']").each(function () {
        var href = $(this).attr('href');
        $(this).attr('href', href.replace(/&?type=\d+/, ''));
      });

      // Remove profile anchor
      //$('.page-user- a#profile-capas_member_profile').remove();

      // Add owner class to relevant member
      if ($('#block-views-relevant-content-block-4 .view-id-relevant_content.view-display-id-block_4 .views-row').hasClass('owner')) {
        $('#block-views-relevant-content-block-4 .view-id-relevant_content.view-display-id-block_4').addClass('owner');
      }

    }
  };
})(jQuery);
