<?php

/**
 * @file
 * Display Suite 3 column equal width template.
 */
?>
<<?php print $layout_wrapper; print $layout_attributes; ?> class="ds-3col-equal <?php print $classes;?> clearfix">

  <?php if (isset($title_suffix['contextual_links'])): ?>
  <?php print render($title_suffix['contextual_links']); ?>
  <?php endif; ?>

<div class="row">
  <<?php print $left_wrapper ?> class="col col-height col-md-5 group-left<?php print $left_classes; ?>">
    <?php print $left; ?>
  </<?php print $left_wrapper ?>>

  <<?php print $middle_wrapper ?> class="col col-height col-md-7 group-middle<?php print $middle_classes; ?>">
    <div class="group-middle-inner-wrapper">
      <?php print $middle; ?>
    </div>
  </<?php print $middle_wrapper ?>>
</div>

<div class="row">
  <<?php print $right_wrapper ?> class="group-right<?php print $right_classes; ?>">
    <?php print $right; ?>
  </<?php print $right_wrapper ?>>
</div>

</<?php print $layout_wrapper ?>>

<?php if (!empty($drupal_render_children)): ?>
  <?php print $drupal_render_children ?>
<?php endif; ?>
