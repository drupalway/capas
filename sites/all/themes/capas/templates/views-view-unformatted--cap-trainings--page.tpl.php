<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>

<?php
$group_nr = 1;                  // first group number
$last_row = count($rows) -1;    // last row
$wrapper  = 3;                  // put a wrapper around every 3 rows
?>

<?php if (!empty($title)): ?>
  <h3><?php print $title; ?></h3>
<?php endif; ?>
<div class="col col-md-4"><div class="grid-page-title"></div></div>
<div class="col col-md-8"><div class="header-text"></div></div>
<?php foreach ($rows as $id => $row): ?>
  <?php if ($id % $wrapper == 0) {print '<div class="views-row group group-'.$group_nr.'">'; $i = 0; $group_nr++; } ?>
  <div class="col col-md-4">
    <div class="training-inner-wrapper<?php if (strpos($classes_array[$id], 'owner') !== FALSE) { print ' owner';  } ?>">
      <?php print $row; ?>
    </div>
  </div>
  <?php $i++; if ($i == $wrapper || $id == $last_row) print '</div>'; ?>
<?php endforeach; ?>