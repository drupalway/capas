<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>

<?php
$group_nr = 1;                  // first group number
$last_row = count($rows) -1;    // last row
$wrapper  = 3;                  // put a wrapper around every 3 rows
?>

<?php if (!empty($title)): ?>
  <h3><?php print $title; ?></h3>
<?php endif; ?>
<?php
$count = 0;
if($view = views_get_view('cap_trainings')) {
  $view->set_arguments(array(arg(1)));
  $view->get_total_rows = true;
  $view->execute('block_1');
  if(isset($view->total_rows)) {
    $count = $view->total_rows;
  }
}
?>
<?php if (!empty($count)): ?>
  <div class="col col-md-4">
    <div class="training-inner-wrapper">
      <?php print views_embed_view('cap_trainings', 'block_1'); ?>
    </div>
  </div>
<?php endif; ?>
<?php foreach ($rows as $id => $row): ?>
  <?php if ($id % $wrapper == 0) {print '<div class="views-row group group-'.$group_nr.'">'; $i = 0; $group_nr++; } ?>
  <div class="col col-md-4">
    <div class="event-inner-wrapper<?php if (strpos($classes_array[$id], 'owner') !== FALSE && arg(0) != 'user') { print ' owner';  } ?>">
      <div class="event-inner-wrapper-hover">
        <?php print $row; ?>
      </div>
    </div>
  </div>
  <?php $i++; if ($i == $wrapper || $id == $last_row) print '</div>'; ?>
<?php endforeach; ?>